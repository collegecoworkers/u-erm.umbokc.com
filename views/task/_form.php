<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Worker;
?>

<div class="row">
	<div class="col-sm-12 col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading no-collapse">Заполните форму</div>
			<div class="" style="margin: 1em;">
				<?php $form = ActiveForm::begin(); ?>

				<?= $form->field($model, 'title')->textInput() ?>
				<?= $form->field($model, 'desc')->textInput() ?>
				<?= $form->field($model, 'date')->textInput(['placeholder' => '2000-12-31']) ?>
				<?= $form->field($model, 'status')->dropDownList($model->getStatuses()) ?>
				<?= $form->field($model, 'user_id')->dropDownList(Worker::getAllArr()) ?>

				<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success' ]) ?>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
