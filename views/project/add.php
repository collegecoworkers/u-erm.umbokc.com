<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Добавить проект';
$this->params['breadcrumbs'][] = ['/'. $this->context->id . '/index', 'Проекты'];
$this->params['breadcrumbs'][] = ['/'. $this->context->route, $this->title];

?>

<?= $this->render('_form', [
	'proj_users' => $proj_users,
	'model' => $model,
]) ?>