<?php

$menu = [];

$menu[] = [ 'label' => 'Главная', 'icon' => 'dashboard', 'url' => "/site/index", ];
$menu[] = [ 'label' => 'Сотрудники', 'icon' => 'users', 'url' => "/worker/index", ];
$menu[] = [ 'label' => 'Проекты', 'icon' => 'desktop', 'url' => "/project/index", ];
$menu[] = [ 'label' => 'Задания', 'icon' => 'bar-chart-o', 'url' => "/task/index", ];
$menu[] = [ 'label' => 'Должности', 'icon' => 'qrcode', 'url' => "/position/index", ];
$menu[] = [ 'label' => 'Пользователи', 'icon' => 'users', 'url' => "/user/index", ];

?>
<nav class="navbar-default navbar-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav" id="main-menu">
			<?php foreach ($menu as $item): ?>
				<li>
					<a class="<?= (ltrim($item['url'], '/') == $this->context->route) ? 'active-menu' : '' ?>" href="<?= $item['url'] ?>">
						<i class="fa fa-<?= $item['icon'] ?>"></i> <?= $item['label'] ?>
					</a>
				</li>
			<?php endforeach ?>
		</ul>
	</div>
</nav>
