<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="row">
	<div class="col-sm-12 col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading no-collapse">Заполните форму</div>
			<div class="" style="margin: 1em;">
				<?php $form = ActiveForm::begin(); ?>

				<?= $form->field($model, 'name')->textInput() ?>
				<?= $form->field($model, 'email')->textInput() ?>
				<?= $form->field($model, 'password')->passwordInput() ?>
				<?= $form->field($model, 'is_admin')->dropDownList(['0' => 'Нет', '1' => 'Да' ]) ?>

				<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success' ]) ?>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
