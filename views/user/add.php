<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Добавление пользователя';
$this->params['breadcrumbs'][] = ['/'. $this->context->id . '/index', 'Пользователи'];
$this->params['breadcrumbs'][] = ['/'. $this->context->route, $this->title];

?>

<?= $this->render('_form', [
	'model' => $model,
]) ?>