<?php 
use app\models\ProjectUser;
use app\models\Worker;

$this->title = 'Главная';
?>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">
				Последние задания
			</div> 
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Дата</th>
								<th>Статус</th>
								<th>Ответственный</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($tasks as $item): ?>
								<?php $worker = Worker::findOne($item->user_id)->full_name; ?>
								<tr>
									<td><?= $item->id ?></td>
									<td><?= $item->title ?></td>
									<td><?= date('m/d/Y', strtotime($item->date)) ?></td>
									<td><?= $item->getStatus() ?></td>
									<td><?= $worker ?></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>


<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">
				Последние проекты
			</div> 
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Дата</th>
								<th>Статус</th>
								<th>Кол-во участников</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($projects as $item): ?>
								<?php $users = ProjectUser::getUsersCount($item->id); ?>
								<tr>
									<td><?= $item->id ?></td>
									<td><?= $item->title ?></td>
									<td><?= date('m/d/Y', strtotime($item->date)) ?></td>
									<td><?= $item->getStatus() ?></td>
									<td><?= $users ?></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>
