<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;

PublicAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>

</head>
<body>
  <?php $this->beginBody() ?>
  
  <div id="wrapper">
    <?= $this->render('/partials/header');?>
    <div id="page-wrapper" style="margin-left: 0">
      <div id="page-inner">
        <?= $content ?>
        <?= $this->render('/partials/footer');?>
      </div>
    </div>
  </div>

  <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
