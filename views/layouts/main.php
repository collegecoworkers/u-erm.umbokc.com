<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;

PublicAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

</head>
<body>
	<?php $this->beginBody() ?>
	
	<div id="wrapper">
		<?= $this->render('/partials/header');?>
		<?= $this->render('/partials/sidebar');?>
		<div id="page-wrapper">
			<div id="page-inner">
				<div class="row">
					<div class="col-md-12">
						<h1 class="page-header">
							<?= $this->title ?>
						</h1>
						<ol class="breadcrumb">
							<li><a href="/">Главная</a></li>
							<?php if (isset($this->params['breadcrumbs'])): ?>
								<?php foreach ($this->params['breadcrumbs'] as $item): ?>
									<li><a href="<?= $item[0] ?>"><?= $item[1] ?></a></li>
								<?php endforeach ?>
							<?php endif ?>
						</ol>
					</div>
				</div>
				<?= $content ?>
				<?= $this->render('/partials/footer');?>
			</div>
		</div>
	</div>

	<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
