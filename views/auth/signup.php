<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
?>

<div class="row">
	<div class="col-md-12">
		<h1 class="page-header">
			<?= $this->title ?>
		</h1>
	</div>
</div> 
<!-- /. ROW  -->
<div class="row">
	<div class="col-lg-6 col-lg-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				Заполните поля
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-10 col-lg-offset-1">
						<?php $form = ActiveForm::begin(); ?>

						<?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя'])?>
						<?= $form->field($model, 'email')->textInput(['placeholder' => 'email'])?>
						<?= $form->field($model, 'password')->passwordInput(['placeholder' => 'пароль'])?>

						<?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-default' ]) ?>
						<?= Html::a('Войти', ['auth/login'], []) ?>
						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
