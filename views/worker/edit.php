<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Изменение данных о сотруднике';
$this->params['breadcrumbs'][] = ['/'. $this->context->id . '/index', 'Сотрудники'];
$this->params['breadcrumbs'][] = ['/'. $this->context->route, $this->title];

?>

<?= $this->render('_form', [
	'model' => $model,
]) ?>
