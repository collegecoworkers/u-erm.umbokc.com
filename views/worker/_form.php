<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Position;

?>

<div class="row">
	<div class="col-sm-12 col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading no-collapse">Заполните форму</div>
			<div class="" style="margin: 1em;">
				<?php $form = ActiveForm::begin(); ?>

				<?= $form->field($model, 'full_name')->textInput() ?>
				<?= $form->field($model, 'birthdate')->textInput(['placeholder' => '2000-12-31']) ?>
				<?= $form->field($model, 'phone')->textInput(['placeholder' => '+12345678901']) ?>
				<?= $form->field($model, 'image')->fileInput() ?>
				<?= $form->field($model, 'status')->dropDownList($model->getStatuses()) ?>
				<?= $form->field($model, 'user_id')->dropDownList(User::getAllArr()) ?>
				<?= $form->field($model, 'position_id')->dropDownList(Position::getAllArr()) ?>

				<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success' ]) ?>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
