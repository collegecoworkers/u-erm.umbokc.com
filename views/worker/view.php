<?php 
use app\models\Position;
use app\models\User;

$this->title = 'Сотрудник';
$cntrl = '/' . $this->context->id;
$this->params['breadcrumbs'][] = ['/'. $this->context->id . '/index', 'Сотрудники'];
?>

<div class="row">

	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Информация
			</div>
			<div class="panel-body">
				<img src="<?= $model->getImage() ?>" width=150 height=150 alt="">
				<p><strong>Имя: </strong> <?= $model->full_name ?></p>
				<p><strong>Дата рождения: </strong> <?= $model->birthdate ?></p>
				<p><strong>Телефон: </strong> <?= $model->phone ?></p>
				<p><strong>Статус: </strong> <?= $model->getStatus() ?></p>
				<p><strong>Пользователь: </strong> <?= $model->getUser()->name ?></p>
				<p><strong>Должность: </strong> <?= $model->getPosition()->title ?></p>
			</div>
		</div>
	</div>
</div>