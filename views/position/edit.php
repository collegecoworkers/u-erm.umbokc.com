<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Изменить должности';
$this->params['breadcrumbs'][] = ['/'. $this->context->id . '/index', 'Должности'];
$this->params['breadcrumbs'][] = ['/'. $this->context->route, $this->title];

?>

<?= $this->render('_form', [
	'model' => $model,
]) ?>
