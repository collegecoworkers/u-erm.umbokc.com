<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="row">
	<div class="col-sm-12 col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading no-collapse">Заполните форму</div>
			<div class="" style="margin: 1em;">
				<?php $form = ActiveForm::begin(); ?>

				<?= $form->field($model, 'title')->textInput() ?>

				<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success' ]) ?>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
