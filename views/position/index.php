<?php 
use app\models\Position;

$this->title = 'Должности';
$cntrl = '/' . $this->context->id;
$this->params['breadcrumbs'][] = ['/'. $this->context->route, $this->title];
?>

<div class="btn-toolbar list-toolbar">
	<a href="<?= $cntrl ?>/add" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить</a>
</div>
<div class="row">
	<div class="col-sm-12 col-md-12">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Наименование</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($items as $item): ?>
					<tr>
						<td><?= $item->id ?></td>
						<td><?= $item->title ?></td>
						<td>
							<a href="<?= $cntrl ?>/edit/?id=<?= $item->id ?>"><i class="fa fa-pencil"></i></a>
							<a href="<?= $cntrl ?>/delete/?id=<?= $item->id ?>" onclick="return confirm('Вы уверенны?')"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
