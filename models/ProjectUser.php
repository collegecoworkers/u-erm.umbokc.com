<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class ProjectUser extends \yii\db\ActiveRecord
{

	public $user_ids = [];

	public static function tableName()
	{
		return 'project_user';
	}

	public function rules()
	{
		return [
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'user_id' => 'Покупатель',
			'project_id' => 'Продукт',
		];
	}

	public static function getUsersCount($proj)
	{
		return self::find()->where(['project_id' => $proj])->count();
	}

	public static function getByProjectOne($proj)
	{
		return self::find()->where(['project_id' => $proj])->one();
	}

	public static function getByProject($proj)
	{
		return self::find()->where(['project_id' => $proj])->all();
	}

	public static function delAll($proj)
	{
		\Yii::$app->db->createCommand()
		->delete(self::tableName(), ['project_id' => $proj])
		->execute();
	}

	public function setUserIds()
	{
		$users_ids = self::find()->where(['project_id' => $this->project_id])->all();
		$users_ids = ArrayHelper::getColumn($users_ids, 'user_id');
		$this->user_ids = $users_ids;
	}

	public function getUsers()
	{
		$users_ids = self::find()->where(['project_id' => $this->project_id])->all();
		$users_ids = ArrayHelper::getColumn($users_ids, 'user_id');
		$users = Worker::find()->where(['id' => $users_ids])->all();
		$users = ArrayHelper::map($users, 'id', 'full_name');
		return $users;
	}

	public function getProject(){
		return Project::find()->where(['id' => $this->project_id])->one();
	}

}
