<?php

namespace app\models;

use Yii;

class Order extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'order';
	}

	public function rules()
	{
		return [
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'user_id' => 'Покупатель',
			'product_id' => 'Продукт',
			'date' => 'Дата',
		];
	}

	public function getByProduct($product_id)
	{
		return self::find()->where(['product_id' => $product_id])->all();
	}

	public function getProduct(){
		return Product::find()->where(['id' => $this->product_id])->one();
	}

}
