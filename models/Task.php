<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

class Task extends \yii\db\ActiveRecord
{

	public static function tableName()
	{
		return 'task';
	}

	public function rules()
	{
		return [
			[['title'], 'required'],
			[['desc'], 'string'],
			[['date'], 'date', 'format' => 'php:Y-m-d'],
			[['user_id', 'status'], 'number'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Название',
			'desc' => 'Описание',
			'date' => 'Дата',
			'status' => 'Статус',
			'user_id' => 'Пользователь',
		];
	}

	public function getStatus(){
		return $this->getStatusBy($this->status);
	}

	public function getStatusBy($status){
		return $this->getStatuses()[$status];
	}

	public function getStatuses(){
		return [
			'В процессе',
			'Готово',
			'Отменено',
			'Не выполнено',
		];
	}

	public static function getAll($lim = null)
	{
		if($lim == null)
			return self::find()->orderBy(['id' => SORT_DESC])->all();
		else
			return self::find()->limit($lim)->orderBy(['id' => SORT_DESC])->all();
	}
}
