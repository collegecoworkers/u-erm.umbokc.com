<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

class User extends \yii\db\ActiveRecord implements IdentityInterface
{
	public static function tableName()
	{
		return 'user';
	}

	public function rules()
	{
		return [
			[['is_admin'], 'integer'],
			[['name', 'email', 'password'], 'string', 'max' => 255],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Name',
			'email' => 'Email',
			'password' => 'Пароль',
			'is_admin' => 'Это Админ',
		];
	}

	public static function findIdentity($id)
	{
		return User::findOne($id);
	}
	public function getId()
	{
		return $this->id;
	}

	public function getAuthKey()
	{
		// TODO: Implement getAuthKey() method.
	}

	public function validateAuthKey($authKey)
	{
		// TODO: Implement validateAuthKey() method.
	}

	public static function findIdentityByAccessToken($token, $type = null)
	{
		// TODO: Implement findIdentityByAccessToken() method.
	}

	public static function findByEmail($email)
	{
		return User::find()->where(['email'=>$email])->one();
	}

	public function validatePassword($password)
	{
		return ($this->password == $password) ? true : false;
	}
	
	public function create()
	{
		return $this->save(false);
	}

	public static function is_admin($uid = null)
	{
		$user = User::findOne($uid == null ? Yii::$app->user->id : $uid);
		return $user->is_admin == 1;
	}

	public static function now()
	{
		$user = User::findOne(Yii::$app->user->id);
		return $user;
	}

	public static function getAll()
	{
		return self::find()->orderBy(['id' => SORT_DESC])->all();
	}

	public static function getAllArr()
	{
		$all = self::find()->all();
		return ArrayHelper::map($all, 'id', 'name');
	}
}
