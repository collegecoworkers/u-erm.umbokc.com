<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class Position extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'position';
	}

	public function rules()
	{
		return [
			[['title'], 'string', 'max' => 255],
			[['desc'], 'string'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Название',
			'desc' => 'Описание',
		];
	}

	public static function getAll()
	{
		return self::find()->orderBy(['id' => SORT_DESC])->all();
	}

	public static function getAllArr()
	{
		$cats = self::find()->all();
		return ArrayHelper::map($cats, 'id', 'title');
	}
}
