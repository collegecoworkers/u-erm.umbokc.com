<?php

namespace app\models;

use Yii;

class Payment extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'payment';
	}

	public function rules()
	{
		return [
			[['sum', 'card_num', 'card_cvv', 'card_date',], 'required'],
			[['sum'], 'number'],
			[['card_cvv'], 'number', 'max' => 999, 'min' => 100],
			[['card_num'], 'match', 'pattern' => '~[0-9]{4} [0-9]{4} [0-9]{4} [0-9]{4}$~'],
			[['card_date'], 'match', 'pattern' => '~[0-9]{2}/[0-9]{2}$~'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'cash_account' => 'Кошелек',
			'sum' => 'Сумма',
			'card_num' => 'Номер карты',
			'card_cvv' => 'CVV карты',
			'card_date' => 'Дата карты',
		];
	}

}
