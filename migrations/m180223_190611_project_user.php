<?php

use yii\db\Migration;

class m180223_190611_project_user extends Migration {

	public function up() {

		$this->createTable('project_user', [
			'id' => $this->primaryKey(),
			'user_id'=>$this->integer(),
			'project_id'=>$this->integer(),
		]);

	}

	public function down() {
		$this->dropTable('project_user');
	}
}
