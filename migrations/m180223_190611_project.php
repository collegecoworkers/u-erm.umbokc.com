<?php

use yii\db\Migration;

class m180223_190611_project extends Migration {

	public function up() {

		$this->createTable('project', [
			'id' => $this->primaryKey(),
			'title'=>$this->string(),
			'desc'=>$this->text(),
			'status'=>$this->integer()->defaultValue(0),
			'date'=>$this->date(),
		]);

	}

	public function down() {
		$this->dropTable('project');
	}
}
