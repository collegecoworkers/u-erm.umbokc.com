<?php

use yii\db\Migration;

class m180223_190611_task extends Migration {

	public function up() {

		$this->createTable('task', [
			'id' => $this->primaryKey(),
			'title'=>$this->string(),
			'desc'=>$this->text(),
			'status'=>$this->integer()->defaultValue(0),
			'date'=>$this->date(),
			'user_id'=>$this->integer(),
		]);

	}

	public function down() {
		$this->dropTable('task');
	}
}
