<?php

use yii\db\Migration;

class m180223_190611_position extends Migration
{

	public function up()
	{
		$this->createTable('position', [
			'id' => $this->primaryKey(),
			'title'=>$this->string(),
			'desc'=>$this->text(),
		]);
	}

	public function down()
	{
		$this->dropTable('position');
	}
}
