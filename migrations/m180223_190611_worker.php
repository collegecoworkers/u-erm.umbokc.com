<?php

use yii\db\Migration;

class m180223_190611_worker extends Migration
{
	public function up()
	{
		$this->createTable('worker', [
			'id' => $this->primaryKey(),
			'full_name'=>$this->string(),
			'birthdate'=>$this->date(),
			'phone'=>$this->string(),
			'image'=>$this->string(),
			'status'=>$this->integer()->defaultValue(0),
			'user_id'=>$this->integer(),
			'position_id'=>$this->integer(),
		]);
	}

	public function down()
	{
		$this->dropTable('worker');
	}
}
