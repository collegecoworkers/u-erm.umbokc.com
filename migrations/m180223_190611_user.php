<?php

use yii\db\Migration;

class m180223_190611_user extends Migration
{
	public function up()
	{
		$this->createTable('user', [
			'id' => $this->primaryKey(),
			'name'=>$this->string(),
			'email'=>$this->string()->defaultValue(null),
			'password'=>$this->string(),
			'is_admin'=>$this->integer()->defaultValue(0),
		]);
	}

	public function down()
	{
		$this->dropTable('user');
	}
}
