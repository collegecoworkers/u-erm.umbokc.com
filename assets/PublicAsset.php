<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PublicAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	
	public $css = [
		'public/css/bootstrap.css',
		'public/css/font-awesome.css',
		'public/js/morris/morris-0.4.3.min.css',
		'public/css/custom-styles.css',
		'http://fonts.googleapis.com/css?family=Open+Sans',
		"css/site.css",
	];
	
	public $js = [
		'public/js/jquery-1.10.2.js',
		'public/js/bootstrap.min.js',
		'public/js/jquery.metisMenu.js',
		'public/js/morris/raphael-2.1.0.min.js',
		'public/js/morris/morris.js',
		'public/js/easypiechart.js',
		'public/js/easypiechart-data.js',
		'public/js/Lightweight-Chart/jquery.chart.js',
		'public/js/custom-scripts.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
	];
}
