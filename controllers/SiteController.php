<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

use app\models\Task;
use app\models\User;
use app\models\Worker;
use app\models\Project;
use app\models\Position;
use app\models\ProjectUser;
use app\models\ImageUpload;

class SiteController extends Controller {

	public function behaviors() {
		return [
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function beforeAction($action) {

		if (Yii::$app->user->isGuest) {
			return $this->redirect('/auth/login');
		}

		return parent::beforeAction($action);
	}

	public function actionIndex() {
		$tasks = Task::getAll(10);
		$projects = Project::getAll(10);

		return $this->render('index',[
			'projects'=>$projects,
			'tasks'=>$tasks,
		]);
	}
}
