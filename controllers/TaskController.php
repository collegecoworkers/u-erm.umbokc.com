<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

use app\models\Task;
use app\models\Worker;

class TaskController extends Controller {

	public function behaviors() {
		return [
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function beforeAction($action) {

		if (Yii::$app->user->isGuest) {
			return $this->redirect('/auth/login');
		}

		return parent::beforeAction($action);
	}

	public function actionIndex() {
		$items = Task::getAll();

		return $this->render('index',[
			'items'=>$items,
		]);
	}

	public function actionAdd() {

		$model = new Task();


		if($model->load(Yii::$app->request->post())){
			if($model->save()){
				return $this->redirect('/task/index');
			}
		}

		return $this->render('add',[
			'model'=>$model,
		]);
	}

	public function actionEdit($id) {

		$model = Task::findOne($id);

		if($model->load(Yii::$app->request->post())){
			if($model->save()){
				return $this->redirect('/task/index');
			}
		}

		return $this->render('edit',[
			'model'=>$model,
		]);
	}

	public function actionDelete($id) {

		$model = Task::findOne($id);
		$model->delete();

		return $this->redirect(['/task/index']);
	}

}
