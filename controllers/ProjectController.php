<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

use app\models\Project;
use app\models\ProjectUser;
use app\models\Worker;

class ProjectController extends Controller {

	public function behaviors() {
		return [
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function beforeAction($action) {

		if (Yii::$app->user->isGuest) {
			return $this->redirect('/auth/login');
		}

		return parent::beforeAction($action);
	}

	public function actionIndex() {
		$items = Project::getAll();

		return $this->render('index',[
			'items'=>$items,
		]);
	}

	public function actionAdd() {

		$model = new Project();
		$proj_users = new ProjectUser();


		if($model->load(Yii::$app->request->post())){
			if($model->save()){
				$workers = Yii::$app->request->post()['ProjectUser']['user_ids'];
				foreach ($workers as $item) {
					$pr_usr = new ProjectUser();
					$pr_usr->project_id = $model->id;
					$pr_usr->user_id = $item;
					$pr_usr->save();
				}
				return $this->redirect('/project/index');
			}
		}

		return $this->render('add',[
			'proj_users'=>$proj_users,
			'model'=>$model,
		]);
	}

	public function actionEdit($id) {

		$model = Project::findOne($id);
		$proj_users = ProjectUser::getByProjectOne($model->id);
		$proj_users->setUserIds();

		if($model->load(Yii::$app->request->post())){
			$workers = Yii::$app->request->post()['ProjectUser']['user_ids'];
			if($model->save()){
				ProjectUser::delAll($model->id);
				foreach ($workers as $item) {
					$pr_usr = new ProjectUser();
					$pr_usr->project_id = $model->id;
					$pr_usr->user_id = $item;
					$pr_usr->save();
				}
				return $this->redirect('/project/index');
			}
		}

		return $this->render('edit',[
			'proj_users'=>$proj_users,
			'model'=>$model,
		]);
	}

	public function actionDelete($id) {

		$model = Project::findOne($id);
		ProjectUser::delAll($model->id);
		$model->delete();

		return $this->redirect(['/project/index']);
	}

}
