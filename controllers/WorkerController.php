<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

use app\models\Worker;
use app\models\ImageUpload;

class WorkerController extends Controller {

	public function behaviors() {
		return [
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function beforeAction($action) {

		if (Yii::$app->user->isGuest) {
			return $this->redirect('/auth/login');
		}

		return parent::beforeAction($action);
	}

	public function actionIndex() {
		$items = Worker::getAll();

		return $this->render('index',[
			'items'=>$items,
		]);
	}

	public function actionAdd() {

		$model = new Worker();
		$model_iu = new ImageUpload;

		if($model->load(Yii::$app->request->post())){

			$file = UploadedFile::getInstance($model,'image');
			if($file != null)
				$model->image = $model_iu->uploadFile($file, $model->image);

			if($model->save()){
				return $this->redirect('/worker/index');
			}

		}

		return $this->render('add',[
			'model_iu'=>$model_iu,
			'model'=>$model,
		]);
	}

	public function actionView($id) {

		$model = Worker::findOne($id);

		return $this->render('view',[
			'model'=>$model,
		]);
	}

	public function actionEdit($id) {

		$model = Worker::findOne($id);
		$model_iu = new ImageUpload;
		$old_img = $model->image;

		if($model->load(Yii::$app->request->post())){

			$model->image = $old_img;

			$file = UploadedFile::getInstance($model,'image');
			if($file != null)
				$model->image = $model_iu->uploadFile($file, $model->image);

			if($model->save()){
				return $this->redirect('/worker/index');
			}

		}

		return $this->render('edit',[
			'model'=>$model,
		]);
	}

	public function actionDelete($id) {

		$model = Worker::findOne($id);
		$model->delete();

		return $this->redirect(['/worker/index']);
	}

}
